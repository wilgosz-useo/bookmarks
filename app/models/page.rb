class Page < ActiveRecord::Base
  has_many :bookmarks, dependent: :destroy
  
  validates :domain, presence: true, uniqueness: true
  
  extend Search
end
