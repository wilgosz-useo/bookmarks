module Search
  def search(params = {})  
    conditions = []
    
    if params
      conditions << params.keys.reduce('') do |result, key|
        if params[key] != "" && key.to_s != "utf8" && key.to_s != "action" && key.to_s != "controller"
          result + key.to_s + ' LIKE ? AND ' 
        else
          result
        end
      end.chomp('AND ')
      
      params.each do |key, value| 
        conditions << "%#{value}%" if params[key] != "" && key.to_s != "utf8" && 
                                     key.to_s != "action" && key.to_s != "controller" 
      end
    end
    if conditions.length > 1
      find(:all, :conditions => conditions)
    else
      find(:all)
    end
  end
end