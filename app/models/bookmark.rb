class Bookmark < ActiveRecord::Base
  belongs_to :page
  acts_as_taggable
  
  validates :name, uniqueness: true
  validates :short_link, presence: true, uniqueness:  true
  
  extend Search
  
  def fill_params
    domain = self.short_link.split("//").last.split("/").first
    
    agent = Mechanize.new
      page = agent.get(self.short_link)
      
      self.page = Page.find_by_domain(domain)
      self.page ||= Page.create(domain: domain)
      
      self.name = page.title if self.name == ''
      d = page.at('meta[@name="description"]')
      self.description = d[:content] if d
  end
  
  def tinyfy
    url = URI.parse('http://tinyurl.com/')
    res = Net::HTTP.start(url.host, url.port) {|http|
      http.get('/api-create.php?url=' + self.short_link)
     }
     self.short_link = res.body
  end
  
end