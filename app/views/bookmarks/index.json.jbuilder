json.array!(@bookmarks) do |bookmark|
  json.extract! bookmark, :id, :name, :description, :short_link, :page_id
  json.url bookmark_url(bookmark, format: :json)
end
