class CreateBookmarks < ActiveRecord::Migration
  def change
    create_table :bookmarks do |t|
      t.string :name
      t.text :description
      t.string :short_link
      t.references :page, index: true

      t.timestamps
    end
  end
end
